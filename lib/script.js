/**
 * client uses a promotion
 * @param {org.ubirewards.setupDeleteAll} attribution
 * @transaction
 */
async function setupDeleteAll() {
    //Define RuntimeAPI function getFactory
    const factory = getFactory();
    //Define Namespace
    var NS = 'org.ubirewards';

    //Remove Client Registries
    const clientRegistry = await getParticipantRegistry(NS + '.Client');
    const clients = await clientRegistry.getAll();
    for (var i = 0; i < clients.length; i++) {
        const client = clients[i];
        await clientRegistry.remove(client);
    }

    //Remove Promotion Registries
    const promotionRegistries = await getAssetRegistry(NS + '.Promotion');
    const promotions = await promotionRegistries.getAll();
    for (var i = 0; i < promotions.length; i++) {
        const promotion = promotions[i];
        await promotionRegistries.remove(promotion);
    }

    //Remove Company Registries
    const companyRegistry = await getParticipantRegistry(NS + '.Company');
    const companies = await companyRegistry.getAll();
    for (var i = 0; i < companies.length; i++) {
        const companie = companies[i];
        await companyRegistry.remove(companie);
    }

}

/**
 * client uses a promotion
 * @param {org.ubirewards.setupPromotionDemo} attribution
 * @transaction
 */
async function setupPromotionDemo() {
    var NS = 'org.ubirewards';
    var promotionStrings = ['OL1', 'OLA2', 'OLA3', 'OLA4'];
    var factory = getFactory();


    return getParticipantRegistry(NS + '.Company')
        .then(function(companyRegistry) {
            return companyRegistry.getAll()
                .then(async function(allCompanies) {

                    for (var i = 0; i < promotionStrings.length; i++) {
                        /* Create new Promotion */
                        var promotionRegistry = await getAssetRegistry(NS + '.Promotion');
                        var promotionAsset = factory.newResource('org.ubirewards', 'Promotion', promotionStrings[i]);
                        promotionAsset.promotionPercentageAmount = 10;
                        promotionAsset.expirationDate = new Date((new Date()).valueOf() + 1000 * 3600 * 24);;
                        promotionAsset.companyIssuer = allCompanies[0];
                        promotionAsset.tokenAmountPerUse = 10;
                        promotionAsset.clientListThatUsed = new Array();

                        /* Updates Company list of promotions */
                        allCompanies[0].promotionList.push(promotionAsset);
                        await promotionRegistry.add(promotionAsset);
                        await companyRegistry.update(allCompanies[0]);

                        console.log('Promotion was successfully created by the company ' + allCompanies[0].name + ' with the ID: ' + promotionStrings[i]);
                    }


                });
        })



}




/**
 * client uses a promotion
 * @param {org.ubirewards.setupParticipantDemo} attribution
 * @transaction
 */
async function setupParticipantDemo() {

    var clientNames = ['José', 'António', 'Cabaça', 'Alberto']
    var companyNames = ['Transdev', 'Leirifisioterapia', 'Rede Expresso', 'McDonalds']
    var NS = 'org.ubirewards';
    const factory = getFactory();

    for (var i = 0; i < clientNames.length; i++) {

        /* Gets UUID for Client */
        let ID = generateUUID('c-');
        /* Creates new Client */
        let clientRegistry = await getParticipantRegistry(NS + '.Client');
        let clientParticipant = factory.newResource('org.ubirewards', 'Client', ID);
        clientParticipant.name = clientNames[i];
        clientParticipant.balance = 9999;

        await clientRegistry.add(clientParticipant);

        console.log('Client ' + clientParticipant.name + ' was successfully created with the ID: ' + ID);

        ID = generateUUID('e-');
        /* Creates new Client */
        let companyRegistry = await getParticipantRegistry(NS + '.Company');
        let companyParticipant = factory.newResource('org.ubirewards', 'Company', ID);
        companyParticipant.name = companyNames[i];
        companyParticipant.balance = 9999;
        companyParticipant.promotionList = new Array();

        await companyRegistry.add(companyParticipant);
        console.log('Company ' + companyParticipant.name + ' was successfully created with the ID: ' + ID);


    }


}

/**
 * client uses a promotion
 * @param {org.ubirewards.checkIfCanUsePromotion} attribution
 * @transaction
 */
async function checkIfCanUsePromotion(attribution) { // eslint-disable-line no-unused-vars

    var NS = 'org.ubirewards';
    return getAssetRegistry(NS + '.Promotion')

        .then(function(assetRegistry) {
            return assetRegistry.exists(attribution.promotionString);
        })

        .then(function(exists) {
            /* Check if promotion exists */
            if (!exists) {
                throw new Error('Promotion ' + attribution.promotionString + ' does not exist');
            }
            return getAssetRegistry(NS + '.Promotion')

                .then(function(assetRegistry) {
                    /* Get the asset */
                    return assetRegistry.get(attribution.promotionString);
                })

                .then(function(asset) {

                    /* Assets validity of promotion .. If out of date, change status */
                    if (asset.expirationDate < new Date()) {
                        if (asset.status == 'ACTIVATED') {
                            asset.status = 'EXPIRED';

                            return getAssetRegistry(NS + '.Promotion')

                                .then(function(assetRegistry) {
                                    // Update the asset in the asset registry.
                                    return assetRegistry.update(asset)
                                        .then(function(p) {
                                            throw new Error('Promotion ' + attribution.promotionString + ' has expired');
                                        })
                                })
                        }
                        throw new Error('Promotion ' + attribution.promotionString + ' has expired');
                    } else {
                        /* Promotion is not expired, so check balance of client */
                        if (attribution.client.balance < asset.tokenAmountPerUse) {
                            throw new Error('Client has insuficient funds');
                        }

                        console.log(attribution.client.name + ' is able to use the promotion: ' + attribution.promotionString);

                    }
                })
        })
}

/**
 * client uses a promotion
 * @param {org.ubirewards.usePromotion} attribution
 * @transaction
 */
async function usePromotion(attribution) { // eslint-disable-line no-unused-vars
    var NS = 'org.ubirewards';
    return getAssetRegistry(NS + '.Promotion')

        .then(function(assetRegistry) {
            return assetRegistry.exists(attribution.promotionString);
        })

        .then(function(exists) {
            /* Check if promotion exists */
            if (!exists) {
                throw new Error('Promotion ' + attribution.promotionString + ' does not exist');
            }
            return getAssetRegistry(NS + '.Promotion')

                .then(function(assetRegistry) {
                    /* Get the asset */
                    return assetRegistry.get(attribution.promotionString);
                })

                .then(function(asset) {

                    /* Assets validity of promotion .. If out of date, change status */
                    if (asset.expirationDate < new Date()) {
                        if (asset.status == 'ACTIVATED') {
                            asset.status = 'EXPIRED';

                            return getAssetRegistry(NS + '.Promotion')

                                .then(function(assetRegistry) {
                                    // Update the asset in the asset registry.
                                    return assetRegistry.update(asset);
                                })
                                .then(function(p) {
                                    throw new Error('Promotion ' + attribution.promotionString + ' has expired');
                                })
                        }
                        throw new Error('Promotion ' + attribution.promotionString + ' has expired');
                    } else {
                        /* Promotion is not expired, so check balance of client */
                        if (attribution.client.balance < asset.tokenAmountPerUse) {
                            throw new Error('Client has insuficient funds');
                        }
                        /* Use the promotion successfuly */
                        console.log(attribution.client.name + ' gets ' + asset.promotionPercentageAmount + '% discount on his purchase');
                        /* Withrawal  tokens from client */
                        return getParticipantRegistry(NS + '.Client')

                            .then(function(clientRegistry) {
                                return clientRegistry.get(attribution.client.userID)

                                    .then(function(client) {

                                        client.balance -= asset.tokenAmountPerUse;
                                        clientRegistry.update(client)

                                        /* Company receives tokens */
                                        return getParticipantRegistry(NS + '.Company')

                                            .then(function(companyRegistry) {
                                                return companyRegistry.get(asset.companyIssuer.getIdentifier())

                                                    .then(function(company) {

                                                        company.balance += asset.tokenAmountPerUse;
                                                        companyRegistry.update(company)
                                                        return getAssetRegistry(NS + '.Promotion')

                                                            .then(function(assetRegistry) {
                                                                /* Add client to clientListThatUsed */
                                                                asset.clientListThatUsed.push(client);
                                                                assetRegistry.update(asset);
                                                            })
                                                    })
                                            })
                                    })
                            })




                    }
                })
        })


}

/**
 * rewards to clients after their successful purchase
 * @param {org.ubirewards.rewardTokensToClient} attribution
 * @transaction
 */
async function rewardTokensToClient(attribution) { // eslint-disable-line no-unused-vars

    /* Assert clients purchase was successfull */
    if (attribution.status == 'DECLINED') {
        throw new Error('Clients purchase was declined, cannot provide tokens');
    }

    /* Assert Company has suficient tokens to reward */
    if (attribution.company.balance < attribution.tokenAmountToReward) {
        throw new Error('Company has unsuficient funds');
    }

    const factory = getFactory();

    /* Transfers tokens from company to client */
    const companyRegistry = await getParticipantRegistry('org.ubirewards.Company');
    attribution.company.balance -= attribution.tokenAmountToReward;
    companyRegistry.update(attribution.company);

    const clientRegistry = await getParticipantRegistry('org.ubirewards.Client');
    attribution.client.balance += attribution.tokenAmountToReward;
    clientRegistry.update(attribution.client);

    console.log(attribution.company.name + ' rewarded ' + attribution.client.name + ' with ' + attribution.tokenAmountToReward + ' tokens');

}




/**
 * deploys tokenAmountToDeploy tokens to a company
 * @param {org.ubirewards.deployTokensToCompany} attribution
 * @transaction
 */
async function deployTokensToCompany(attribution) { // eslint-disable-line no-unused-vars

    /* Assert nr of tokens per use is bigger than 0 */
    if (attribution.tokenAmountToDeploy <= 0) {
        throw new Error('Token amount to deploy must be bigger than 0');
    }

    const factory = getFactory();

    /* Updates  tokenAmount value from company */
    const companyRegistry = await getParticipantRegistry('org.ubirewards.Company');
    attribution.company.balance += attribution.tokenAmountToDeploy;
    companyRegistry.update(attribution.company);

    console.log(attribution.tokenAmountToDeploy + ' tokens deployed to Company ' + attribution.company.name);

}

/**
 * issues a new promotion
 * @param {org.ubirewards.issuePromotion} attribution
 * @transaction
 */
async function issuePromotion(attribution) { // eslint-disable-line no-unused-vars

    /* Assert expiration date is nos earlier than current date */
    if (attribution.expirationDate < new Date()) {
        throw new Error('Incorrect Date');
    }

    /* Assert % is valid */
    if (attribution.promotionPercentageAmount > 100 || attribution.promotionPercentageAmount <= 0) {
        throw new Error('Incorrect Discount %');
    }

    /* Assert Voucher String is not empty or bigger than 20 chars is valid */
    if (attribution.promotionString.length == 0 || attribution.promotionString.length > 30) {
        throw new Error('Promotion String must have between 0-30 characters');
    }

    /* Assert nr of tokens per use is bigger than 0 */
    if (attribution.tokenAmountPerUse <= 0) {
        throw new Error('Token amount per use must be bigger than 0');
    }

    const factory = getFactory();
    var NS = 'org.ubirewards';
  	let currentParticipant = getCurrentParticipant();
  	console.log(currentParticipant);


    /* Create new Promotion */
    const promotionRegistry = await getAssetRegistry('org.ubirewards.Promotion');
    const promotionAsset = factory.newResource('org.ubirewards', 'Promotion', attribution.promotionString);
    promotionAsset.promotionPercentageAmount = attribution.promotionPercentageAmount;
    promotionAsset.expirationDate = attribution.expirationDate;
    promotionAsset.companyIssuer = currentParticipant;
    promotionAsset.tokenAmountPerUse = attribution.tokenAmountPerUse;
    promotionAsset.clientListThatUsed = new Array();

    /* Updates Company list of promotions */
    const companyRegistry = await getParticipantRegistry('org.ubirewards.Company');
    currentParticipant.promotionList.push(promotionAsset);

    await promotionRegistry.add(promotionAsset);
    await companyRegistry.update(currentParticipant);

    /* Emit a notification that a Promotion was occurred */
    const promotionCreatedEvent = factory.newEvent(NS, 'PromotionCreated');
    promotionCreatedEvent.promotionString = promotionAsset;
    emit(promotionCreatedEvent);


    console.log('Promotion was successfully created by the company ' + currentParticipant.name + ' with the ID: ' + promotionAsset.promotionString);
}

/**
 * add a new company 
 * @param {org.ubirewards.addCompany} attribution
 * @transaction
 */
async function addCompany(attribution) {

    const factory = getFactory();

    /* Gets UUID for Client */
    const ID = generateUUID('e-');

    /* Create new Company */
    const companyRegistry = await getParticipantRegistry('org.ubirewards.Company');
    const companyParticipant = factory.newResource('org.ubirewards', 'Company', attribution.userID);
    companyParticipant.name = attribution.name;
    companyParticipant.promotionList = new Array();


    await companyRegistry.add(companyParticipant);
    console.log('Company ' + attribution.name + ' was successfully created with the ID: ' + ID);

}

/**
 * add a new client  
 * @param {org.ubirewards.addClient} attribution
 * @transaction
 */
async function addClient(attribution) {
    const factory = getFactory();

    /* Gets UUID for Client */
    const ID = generateUUID('c-');

    /* Creates new Client */
    const clientRegistry = await getParticipantRegistry('org.ubirewards.Client');
    const clientParticipant = factory.newResource('org.ubirewards', 'Client', attribution.userID);
    clientParticipant.name = attribution.name;

    await clientRegistry.add(clientParticipant);

    console.log('Client ' + attribution.name + ' was successfully created with the ID: ' + ID);

}



/********************************************************************
 * 																	*
 *						THIRD PARTY FUNCTIONS					 	*
 *																	*
 *******************************************************************/



/* Generates random Universal Unique Identifier starting with c or e for Client(c) or Enterprise(e) */
function generateUUID(type) {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return type += uuid;
};