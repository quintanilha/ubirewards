#Ubi Reward - Multi agent rewarding system

> Ubireward allows clients and companies to take advantage of blockchain technology, like Hyperledger, on a multi-agent rewarding system. Bringing them transparency and integrity across users, and flexibility on the Business network application.

This business network defines:

**Participants**
`Company` `Client`

**Assets**
`Promotion`

**Transactions**
`addClient`
`addCompany`
`deployTokensToCompany`
`rewardTokensToClient`
`issuePromotion`
`checkIfCanUsePromotion`
`usePromotion`

**Concepts**
`Address`

**Enumeratios**
`PromotionStatus`
`clientPurchaseStatus`

**Events**
`PromotionCreated`

##Business network application documentation
###Participants
**Client** 

Any user who wishes to make use of the rewarding system. Gets rewarded with tokens for normal purchases and spends them in exchange for discounts.

| Type        	| Fields        | Mandatory | Description	|
| ------------- |:-------------:| ---------:| ------------:	|
| String      	| userID 		| 	Yes	 	| Clients' identity	|
| String      	| name	      	|   Yes	 	| Clients' name		|
| Adress 		| address      	|   No		| Clients' Address		|
| Double      	| balance 		| 	Yes	 	| Clients' balance	|


**Company**

Any company who wishes to make use of the rewarding system. Pays money for tokens (prices to be settled) that are then deployed from the Network Administrator. Rewards clients from their normal purchases and issues promotions that can be used by clients.

| Type        	| Fields        | Mandatory | Description	|
| ------------- |:-------------:| ---------:| ------------:	|
| String      	| userID 		| 	Yes	 	| Companies' identity	|
| String      	| name	      	|   Yes	 	| Companies' name		|
| Adress 		| address      	|   No		| Companies' Address		|
| Double      	| balance 		| 	Yes	 	| Companies' balance	|
| Promotion[]   | promotionList | 	Yes	 	| List of Companies' promotions	|

###Assets
**Promotion**

A Promotion issued by a Company targeting any Client. Contains all the information regarding its discounts and expiration date. 

| Type        	| Fields        | Mandatory | Description	|
| ------------- |:-------------:| ---------:| ------------:	|
| String      	| promotionString 		| 	Yes	 	| Promotions Identifier	|
| Integer      	| promotionPercentageAmount	      	|   Yes	 	| % discount to be applied 		|
| PromotionStatus 		| status      	|   Yes		| Promotion is Activated/Expired		|
| DateTime      	| expirationDate 		| 	Yes	 	| Expiration Date	|
| Double     | tokenAmountPerUse | 	Yes	 	| Tokens necessary to use	|
| Company     | companyIssuer | 	Yes	 	| Company that issued	|
| Client[]     	| clientListThatUsed | 	Yes	 	| List of Companies' promotions	|


###Transactions Summary

**addClient()** - Creates a new resource type Client

Parameters -  `string userID`, `string name`

*Procedure*
1. Get Factory
2. Generates / Sets a new unique ID (depends on version)
3. Creates and Adds Resource

Example
```
{
  "$class": "org.ubirewards.addClient",
  "userID": "12345",
  "name": "Manuel"
}
```
**addCompany()** - Creates a new resource type Company

Parameters - `string userID`, `string name`

*Procedure*
1. Get Factory
2. Generates / Sets a new unique ID (depends on version)
3. Creates and Adds Resource

Example
```
{
  "$class": "org.ubirewards.addCompany",
  "userID": "54321",
  "name": "Transdev"
}
```

**deployTokensToCompany()** - Deploy tokens to companies balance

Parameters - `double tokenAmountToDeploy`, `Company company`

*Procedure*
1. Check validations
2. Get Registry 
3. Update balance

Example
```
{
  "$class": "org.ubirewards.deployTokensToCompany",
  "tokenAmountToDeploy": 100,
  "company": "resource:org.ubirewards.Company#1111"
}
```
**rewardTokensToClient()** - Transfers tokens from Company to Client after Successful purchase

Parameters - `double tokenAmountToReward`, `Company company`, `Client client`, `clientPurchaseStatus status`

*Procedure*
1. Check validations
2. Fetch Client and Company Registries
3. Update balances according to values

Example
```
{
  "$class": "org.ubirewards.rewardTokensToClient",
  "tokenAmountToReward": 10,
  "client": "resource:org.ubirewards.Client#2222",
  "company": "resource:org.ubirewards.Company#1111",
  "status": "SUCCESS"
}
```

**issuePromotion()** - Company issues a valid promotion for its products for Client usage

Parameters - `Company companyIssuer`, `Integer promotionPercentageAmount`, `DateTime expirationDate`, `Double tokenAmountPerUse`, `string promotionString`

*Procedure*
1. Check validations
2. Get Fabric and Promotion registry
3. Creates and loads Promotion into registry
4. Emit event promotionCreatedEvent

Example
```
{
  "$class": "org.ubirewards.issuePromotion",
  "companyIssuer": "resource:org.ubirewards.Company#1111",
  "promotionPercentageAmount": 10,
  "expirationDate": "2018-09-30T18:26:45.328Z",
  "tokenAmountPerUse": 100,
  "promotionString": "Transdev10"
}
```

**checkIfCanUsePromotion()** - Check the validity of a promotion and if a Client has enough funds to use it

Parameters - `Client client`, `string promotionString`

*Procedure*
1. Check if Promotion exists
2. Check if Promotion is valid 
3. Check relationship between Clients funds and Promotions token amount to be used
4. Validates and gives access to usePromotion()

Example
```
{
  "$class": "org.ubirewards.checkIfCanUsePromotion",
  "client": "resource:org.ubirewards.Client#2222",
  "promotionString": "Transdev10"
}
```


**usePromotion()** - Client uses a promotion 

Parameters - `Client client`, `string promotionString`

*Procedure*
1. Check validations
2. Fetch current Client and Promotion tokens to be used
3. Updates balances from Client and Company that issued the promotion

Example
```
{
  "$class": "org.ubirewards.usePromotion",
  "client": "resource:org.ubirewards.Client#2222",
  "promotionString": "Transdev10"
}
```
###Testing porpuses

**setupParticipantDemo()** - Creates 4 Clients and 4 Companies and deploys 9999 tokens to all

**setupPromotionDemo()** - Creates 4 promotions issued by the first Company in list

**setupDeleteAll()** - Deletes every participant

