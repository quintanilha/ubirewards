/**
 * 
 * Use this as a template for unit test cases
 * 
 */

var assert = require('chai').assert;
var expect = require('chai').expect;


const utHarness = require('../util-version0.01/ut-harness');

// This points to the model project folder
var modelFolder = 'C:/Users/Quintanilha/Desktop/ubirewards';

var adminConnection = {}
var businessNetworkConnection = {}
var bnDefinition = {}


// Synchronous call so that connections can be established
before((done) => {
    utHarness.debug = false;
    utHarness.initialize(modelFolder, (adminCon, bnCon, definition) => {
        adminConnection = adminCon;
        businessNetworkConnection = bnCon;
        bnDefinition = definition;
        done();
    });
})

const nameSpace = 'org.ubirewards';
const participantClient = 'Client';
const participantCompany = 'Company';
const addClient = "addClient";
const addCompany = "addCompany";
const deployTokensToCompany = "deployTokensToCompany";



// Test Suite # 1
describe('Unit Testing for BNA Ubirewards!', () => {

    // Synchronous - Initialize the network on every test
    beforeEach((done) => {
        utHarness.debug = false;
        utHarness.initialize(modelFolder, (adminCon, bnCon, definition) => {
            adminConnection = adminCon;
            businessNetworkConnection = bnCon;
            bnDefinition = definition;
            done();
        });
    });

    // Test Case # 1
    it('Test case # 1 - Client must be initialized with 0 Points on his balance', async () => {
        // 1. Get the factory
        let factory = bnDefinition.getFactory();
        // 2. Create an instance of transaction
        let options = {
            generate: false,
            includeOptionalFields: false
        }
        let userID = "1234";
        let transaction = factory.newTransaction(nameSpace,addClient,userID,options);


        // 3. Set up the properties of the transaction object
        transaction.setPropertyValue('userID','1234');
        transaction.setPropertyValue('name','Dummy Client');

        // 4. Submit the transaction
        await businessNetworkConnection.submitTransaction(transaction);

        // 5. Get participant
        const participantRegistry = await businessNetworkConnection.getParticipantRegistry(nameSpace + '.' + participantClient);
        const participant1 = await participantRegistry.getAll();
        //console.log(participant1[0].getIdentifier());

        // 6. Validate
        expect(participant1[0].balance == 0).to.equal(true);
        

        });

        // Test Case # 2
    it('Test case # 2 - Company must be initialized with 0 Points on its balance', async () => {
        // 1. Get the factory
        let factory = bnDefinition.getFactory();

        // 2. Create an instance of transaction
        let options = {
            generate: false,
            includeOptionalFields: false
        }
        let userID = "1234";
        let transaction = factory.newTransaction(nameSpace,addCompany,userID,options);

        // 3. Set up the properties of the transaction object
        transaction.setPropertyValue('userID','4321');        
        transaction.setPropertyValue('name','Dummy Company');

        // 4. Submit the transaction
        await businessNetworkConnection.submitTransaction(transaction);

        // 5. Get participant
        const participantRegistry = await businessNetworkConnection.getParticipantRegistry(nameSpace + '.' + participantCompany);
        const participant1 = await participantRegistry.getAll();

       
        // 6. Validate
        expect(participant1[0].balance == 0).to.equal(true);

        }); 

         // Test Case # 3
    it('Test case # 3 - Company must have X points after X points deployed to it', async () => {
        // 1. Get the factory
        let factory = bnDefinition.getFactory();

        // 2. Create an instance of transaction
        let options = {
            generate: false,
            includeOptionalFields: false
        }
        let userID = "1234";
        let transaction = factory.newTransaction(nameSpace,addCompany,userID,options);

        // 3. Set up the properties of the transaction object
        transaction.setPropertyValue('userID','4321');        
        transaction.setPropertyValue('name','Dummy Company');

        // 4. Submit the transaction
        await businessNetworkConnection.submitTransaction(transaction);

        ///////////////

        let options = {
            generate: false,
            includeOptionalFields: false
        }
        let userID = "1234";
        let transaction = factory.newTransaction(nameSpace,deployTokensToCompany,userID,options);

        transaction.setPropertyValue('tokenAmountToDeploy','1000');        
        transaction.setPropertyValue('name','Dummy Company');

    }); 
});