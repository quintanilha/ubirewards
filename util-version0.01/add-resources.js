'use strict';
/**
 * 
 * Sample shows how to use asset registry for adding new instances 
 * of resources.
 * 
 * 
 * Execution environment
 * =====================
 * 1. Fabric runtime is up
 * 2. ubirewards deployed
 * 
 * Demo Steps
 * ==========
 * 1. Connect using the bn-connection-util
 * 2. Get the AssetRegistry from connection
 * 3. Create 2 instances of Client resource using the factory & initialize
 * 4. Invoke registry.addAll([Array of Clients resource instances])
 */

const namespace = 'org.ubirewards';
const participantType = 'Client';

// 1. Connect
const bnUtil = require('./bn-connection-util');
bnUtil.connect(main);

function main(error){
    // Check for the connection error
    if(error){
        console.log(error);
        process.exit(1);
    }

    // 2. Get the Client AssetRegistry
    return bnUtil.connection.getParticipantRegistry(namespace+'.'+participantType).then((registry)=>{
        console.log('1. Received Registry: ', registry.id);

        // Utility method for adding the aircrafts
        addClients(registry);

    }).catch((error)=>{
        console.log(error);
       // bnUtil.disconnect();
    });
}

/**
 * Creates two resources 
 * @param {*} registry This is of type AssetRegistry
 */
function    addClients(registry){
    // 3. This Array will hold the instances of client resource
    let    clients = [];
    const  bnDef = bnUtil.connection.getBusinessNetwork();
    const  factory = bnDef.getFactory();

    // Instance#1
    let    clientResource = factory.newResource(namespace,participantType,'12345');
    clientResource.setPropertyValue('name','Roberto');
    // Push instance to  the aircrafts array
    clients.push(clientResource);

    // Instance#2 
    clientResource = factory.newResource(namespace,participantType,'54321');
    // also duable instead of using the setPropertyValue()
    clientResource.name='Tiago';

    // Push instance to  the aircrafts array
    clients.push(clientResource);

    // 4. Add the Aircraft resource to the registry
    return registry.addAll(clients).then(()=>{
        console.log('Added the Resources successfully!!!');
        bnUtil.disconnect();
    }).catch((error)=>{
        console.log(error);
        bnUtil.disconnect();
    });
}
