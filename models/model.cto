namespace org.ubirewards

/* Participants */
abstract participant User identified by userID{
  o String userID
  o String name
  o Address address optional
  o Double balance default = 0.0
}

participant Company extends User{
  --> Promotion[] promotionList optional
}

participant Client extends User{
}

/* Assets */
asset Promotion identified by promotionString{
  o String promotionString
  o Integer promotionPercentageAmount
  o PromotionStatus status default = 'ACTIVATED'
  o DateTime expirationDate
  o Double tokenAmountPerUse
  --> Company companyIssuer
  --> Client[] clientListThatUsed
}


/* Transactions */
  
transaction addClient{
 	o String userID
  	o String name
  	o Address address optional
}
  
transaction addCompany{
  	o String userID
  	o String name
  	o Address address optional
}
  
transaction deployTokensToCompany{
  	o Double tokenAmountToDeploy
  	--> Company company
}

transaction issuePromotion{
  --> Company companyIssuer optional
  o Integer promotionPercentageAmount
  o DateTime expirationDate
  o Double tokenAmountPerUse
  o String promotionString
}

transaction rewardTokensToClient{
    o Integer tokenAmountToReward
    --> Client client
    --> Company company
    o clientPurchaseStatus status
}

transaction usePromotion{
  --> Client client
  o String promotionString
}

transaction checkIfCanUsePromotion {
  --> Client client
  o String promotionString
}
  
transaction  setupParticipantDemo{

}
  
transaction  setupPromotionDemo{

}
  
transaction  setupDeleteAll{

}
  
/* Events */
event PromotionCreated{
	--> Promotion promotionString
}

/* Enumerations */
enum PromotionStatus {
  o ACTIVATED
  o EXPIRED
}

enum clientPurchaseStatus {
  o SUCCESS
  o DECLINED
}

/* Concepts */
concept Address {
  o String city
  o String country
  o String street
  o String zip
}
